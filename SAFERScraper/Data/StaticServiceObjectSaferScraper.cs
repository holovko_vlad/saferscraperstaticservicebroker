﻿using System;
using System.Collections.Generic;
using System.Text;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;


namespace SAFERScraper.Data
{
    /// <summary>
    /// Sample implementation of a Static Service Object.
    /// The class is decorated with a ServiceObject Attribute providing definition information for the Service Object.
    /// The Properties and Methods in the class are each decorated with attributes that describe them in Service Object terms
    /// This sample implementation contains two Properties (Number and Text) and two Methods (Read and List)
    /// </summary>
    [Attributes.ServiceObject("StaticServiceObjectSafer", "Static Service Object SaferScraper (DisplayName)", "Static Service Object SaferScraper (Description)")]
    class StaticServiceObjectSaferScraper
    {
	
		/// <summary>
        /// This property is required if you want to get the service instance configuration 
        /// settings in this class
        /// </summary>
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }
		
        #region Class Level Fields

        #region Private Fields
        private string usdot = "";
        private string mcmx = "";
        private string name = "";
        private string result = "";
        #endregion

        #endregion

        #region Properties with Property Attribute

        #region string usdot
        /// <summary>
        /// An integer representing property1.
        /// The property is decorated with a Property Attribute which describes the Service Object Property
        /// </summary>
        [Attributes.Property("USDOT", SoType.Text, "USDOT (Display Name)", "USDOT (Description)")]
        public string USDOT
        {
            get { return usdot; }
            set { usdot = value; }
        }
        #endregion

        #region string MCMX
        /// <summary>
        /// An integer representing property2.
        /// The property is decorated with a Property Attribute which describes the Service Object Property
        /// </summary>
        [Attributes.Property("MCMX", SoType.Text, "MCMX (Display Name)", "MCMX (Description)")]
        public string MCMX
        {
            get { return mcmx; }
            set { mcmx = value; }
        }
        #endregion

        #region string Name
        /// <summary>
        /// An integer representing property2.
        /// The property is decorated with a Property Attribute which describes the Service Object Property
        /// </summary>
        [Attributes.Property("Name", SoType.Text, "Name (Display Name)", "Name (Description)")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        #region string Result
        /// <summary>
        /// An integer representing property2.
        /// The property is decorated with a Property Attribute which describes the Service Object Property
        /// </summary>
        [Attributes.Property("Result", SoType.Text, "Result (Display Name)", "Result (Description)")]
        public string Result
        {
            get { return result; }
            set { result = value; }
        }
        #endregion

        #endregion
        #endregion

        #region Default Constructor
        /// <summary>
        /// Instantiates a new ServiceObject1.
        /// </summary>
        public StaticServiceObjectSaferScraper()
        {
            // No implementation necessary.
        }
        #endregion

        #region Methods with Method Attribute

        #region GetInfo(Property[] inputs, RequiredProperties required, Property[] returns, MethodType methodType, ServiceObject serviceObject)
        /// <summary>
        /// Sample implementation of a static service Object Read method
        /// The method is decorated with a Method Attribute which describes the Service Object Method.
        /// </summary>
        [Attributes.Method("GetInfo", MethodType.Read, "GetInfo - Read (Display Name)", "GetInfo - Read (Description)",
            new string[] { "" }, //required property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "USDOT", "MCMX", "Name" }, //input property array. Properties must match the names of Properties decorated with the Property Attribute
            new string[] { "Result" })] //return property array. Properties must match the names of Properties decorated with the Property Attribute
        public StaticServiceObjectSaferScraper GetInfo()
        {
            string[] companyParams = new string[3];
            
            if (!String.IsNullOrEmpty(USDOT))
            {
                companyParams = Helper.GetAllParametersByParameter("USDOT", USDOT);
            }
            else if (!String.IsNullOrEmpty(MCMX))
            {
                companyParams = Helper.GetAllParametersByParameter("MC_MX", MCMX);
            }
            else if (!String.IsNullOrEmpty(Name))
            {
                companyParams = Helper.GetAllParametersByParameter("NAME", Name);
            }

            this.Result = "USDOT: " + companyParams[0] + ", MC_MX: " + companyParams[1] + ", Legal Name: " + companyParams[2];

            /*
            string USDOT = "1765143";
            string MCMX = "717315";
            string Name = "ALL KNIGHT TRANSPORTATION LLC";
            */

            return this;
        }
        #endregion

        #endregion

        #region Helper Methods

        #endregion
    }
}