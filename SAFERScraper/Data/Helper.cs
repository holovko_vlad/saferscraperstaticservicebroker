﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using CsQuery;
using System.Text.RegularExpressions;

namespace SAFERScraper.Data
{
    class Helper
    {


        public static string[] GetAllParametersByParameter(string param, string value)
        {
            string result = "";
            string[] companyParams = new string[3];

            if (param == "USDOT")
            {
                result = GetHTMLByParameterAsync("USDOT", value).Result;
                companyParams = GetParamsFromHTML(result);
            }
            else if (param == "MC_MX")
            {
                result = GetHTMLByParameterAsync("MC_MX", value).Result;
                companyParams = GetParamsFromHTML(result);
            }
            else if (param == "NAME")
            {
                result = GetHTMLByParameterAsync("NAME", value).Result;

                CQ cq = CQ.Create(result);

                foreach (IDomObject obj in cq.Find("a"))
                {
                    string href = obj.GetAttribute("href");
                    if (href.Length > 5 && href.Substring(0, 5) == "query")
                    {
                        string companyURI = "https://safer.fmcsa.dot.gov/" + href;
                        result = GetHTMLByURIAsync(companyURI).Result;
                    }
                }
                companyParams = GetParamsFromHTML(result);
            }

            return companyParams;
        }

        public static string[] GetParamsFromHTML(string html)
        {
            string[] companyParams = new string[3];
            CQ cq = CQ.Create(html);

            foreach (IDomObject obj in cq.Find("th.querylabelbkg a"))
            {
                string href = obj.GetAttribute("href");
                if (href == "saferhelp.aspx#USDOTID")
                {
                    CQ p = CQ.Create(obj.ParentNode.ParentNode.Render());
                    IDomObject td = p.Find("td").FirstElement();
                    companyParams[0] = Regex.Replace(td.InnerText, @"<[^>]+>|&nbsp;", "").Trim();
                }
                else if (href == "saferhelp.aspx#ICCNumbers")
                {
                    CQ p = CQ.Create(obj.ParentNode.ParentNode.Render());
                    IDomObject td = p.Find("td").FirstElement().ChildElements.First();
                    companyParams[1] = Regex.Replace(td.InnerText, @"<[^>]+>|&nbsp;", "").Trim();
                }
                else if (href == "saferhelp.aspx#Carrier")
                {
                    CQ p = CQ.Create(obj.ParentNode.ParentNode.Render());
                    IDomObject td = p.Find("td").FirstElement();
                    companyParams[2] = Regex.Replace(td.InnerText, @"<[^>]+>|&nbsp;", "").Trim();
                }

            }

            return companyParams;
        }

        public static async Task<string> GetHTMLByParameterAsync(string param, string value)
        {
            string responseString = "";

            HttpClient client = new HttpClient();

            var values = new Dictionary<string, string>
                {
                    { "searchtype", "ANY" },
                    { "query_type", "queryCarrierSnapshot" },
                    { "query_param", param },
                    { "query_string", value }
                };

            var content = new FormUrlEncodedContent(values);

            HttpResponseMessage response = await client.PostAsync("https://safer.fmcsa.dot.gov/query.asp", content);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                responseString = await response.Content.ReadAsStringAsync();
            }
            else
            {
                responseString = "error";
            }

            return responseString;
        }

        public static async Task<string> GetHTMLByURIAsync(string URI)
        {
            string responseString = "";

            HttpClient client = new HttpClient();

            HttpResponseMessage response = await client.GetAsync(URI);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                responseString = await response.Content.ReadAsStringAsync();
            }
            else
            {
                responseString = "error";
            }

            return responseString;
        }
    }
}
